import React from 'react';
import {Image, Text, View, StyleSheet } from 'react-native';

const App = () =>{
  return(    
    <View style={{ alignItems: "center", justifyContent: "center", marginTop: 40, marginRight: 75, marginLeft: 75}}>
        <View style={{ backgroundColor: "#eee", borderRadius: 20, overflow: "hidden" }}>
          <View>
            <Image
            style= {styles.image}
            source={{uri: 'https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png',}}/>
          </View>
          <View style={{ padding: 10}}>
            <Text style={{alignSelf: 'center', fontWeight: 'bold', color: 'black' }}>Styling Di React Native</Text>
            <Text style={{ color: "#777", paddingTop: 5 , alignSelf: 'center' }}>
              React Native - Binar Academy
            </Text>
            <Text style={{ color: "black", paddingTop: 5 , alignSelf: 'center' }}>
              As a component grows in completly, it is much cleaner and efficient to use StyleSheet.create so as to define several styles in one place
            </Text>
            <View style={{ marginTop: 20, flexDirection:'row', justifyContent:'space-around' }}>
              <Text style={{ fontSize: 20, color:'teal' }}>Understood</Text>
              <Text style={{ fontSize: 20, color:'teal' }}>What!!</Text>
            </View>
          </View>
        </View>
    </View>    
  )
}

const styles = StyleSheet.create({
  image: {
    height: 360,
    width: 370,
  }
});

export default App;
